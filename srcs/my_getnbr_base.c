/*
** my_getnbr_base.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:25:17 2016 Matthias RIGAUD
** Last update Mon May  9 15:25:18 2016 Matthias RIGAUD
*/

# include <raytracer.h>

int	test(char str, char *base)
{
  int i;

  i = 0;
  while (base[i] != 0)
    {
      if (base[i] == str || str == 43 || str == 45)
	{
	  return (1);
	}
      i = i + 1;
    }
  return (0);
}

int	m_p(int n, int p)
{
  if (p == 0)
    {
      return (1);
    }
  return (n * m_p(n, p - 1));
}

int	m_n(char *base, char str, int base_size)
{
  int	i;

  i = 0;
  while (i != base_size)
    {
      if (base[i] == str)
	{
	  return (i);
	}
      i = i + 1;
    }
  return (0);
}

int	error_o(char *base, int *base_s, int i)
{
  while (base[*base_s] != 0)
    {
      i = 0;
      while (base[i] != 0)
        {
          if ((base[*base_s] == base[i] && *base_s != i) || base[0] == 0)
            {
              return (0);
            }
          i = i + 1;
        }
      *base_s = *base_s + 1;
    }
  return (1);
}

unsigned int	my_getnbr_base(char *str, char *base, int *error)
{
  unsigned int	nbr;
  int		bas_s;
  int		str_s;
  int		i[2];

  bas_s = 0;
  str_s = 0;
  i[1] = 0;
  nbr = 0;
  i[0] = 0;
  if ((error_o(base, &bas_s, 0) == 0 || error_t(str, base, &str_s, i) == 0)
      && (*error = 1))
    return (0);
  while (i[0] != str_s)
    {
      nbr = nbr - (m_p(bas_s, str_s - 1 - i[0]) * m_n(base, str[i[0]], bas_s));
      i[0] = i[0] + 1;
    }
  if (i[1] % 2 == 0)
    {
      nbr = -nbr;
    }
  return (nbr);
}
