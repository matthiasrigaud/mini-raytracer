/*
** blurp.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:22:23 2016 Matthias RIGAUD
** Last update Mon May  9 15:22:32 2016 Matthias RIGAUD
*/

# include <raytracer.h>

int	getxy(int x, int y, t_pixar *pix)
{
  return (y * WB + x);
}

int		moy(t_bunny_pixelarray *pix, int i, int j, int v)
{
  t_color	*pixels;
  int		coef;
  int		som;

  pixels = pix->pixels;
  som = pixels[getxy(i, j, pix)].argb[v];
  coef = 1;
  if (i - 1 >= 0 && j - 1 >= 0 && ++coef)
    som += pixels[getxy(i - 1, j - 1, pix)].argb[v];
  if (i - 1 >= 0 && ++coef)
    som += pixels[getxy(i - 1, j, pix)].argb[v];
  if (i - 1 >= 0 && j + 1 < HB && ++coef)
    som += pixels[getxy(i - 1, j + 1, pix)].argb[v];
  if (j - 1 >= 0 && ++coef)
    som += pixels[getxy(i, j - 1, pix)].argb[v];
  if (j + 1 < HB && ++coef)
    som += pixels[getxy(i, j + 1, pix)].argb[v];
  if (i + 1 < WB && j - 1 >= 0 && ++coef)
    som += pixels[getxy(i + 1, j - 1, pix)].argb[v];
  if (i + 1 < WB && ++coef)
    som += pixels[getxy(i + 1, j, pix)].argb[v];
  if (i + 1 < WB && j + 1 < HB && ++coef)
    som += pixels[getxy(i + 1, j + 1, pix)].argb[v];
  return (som / coef);
}

int	diff_percent(t_color *a, t_color *b)
{
  return (((double)(VA(a->argb[0] - b->argb[0])) * 100 / 255
	   + (double)(VA(a->argb[1] - b->argb[1])) * 100 / 255
	   + (double)(VA(a->argb[2] - b->argb[2])) * 100 / 255) / 3);
}

int		can_blurp(t_pixar *pix, int i, int j, int diff)
{
  t_color	*pixels;
  t_color	center;

  pixels = pix->pixels;
  center.full = pixels[getxy(i, j, pix)].full;
  if (diff_percent(&pixels[getxy(i - 1, j - 1, pix)], &center) > diff)
    return (1);
  if (diff_percent(&pixels[getxy(i + 1, j + 1, pix)], &center) > diff)
    return (1);
  if (diff_percent(&pixels[getxy(i + 1, j - 1, pix)], &center) > diff)
    return (1);
  if (diff_percent(&pixels[getxy(i - 1, j + 1, pix)], &center) > diff)
    return (1);
  if (diff_percent(&pixels[getxy(i - 1, j, pix)], &center) > diff)
    return (1);
  if (diff_percent(&pixels[getxy(i + 1, j, pix)], &center) > diff)
    return (1);
  if (diff_percent(&pixels[getxy(i, j - 1, pix)], &center) > diff)
    return (1);
  if (diff_percent(&pixels[getxy(i, j + 1, pix)], &center) > diff)
    return (1);
  return (0);
}

void		blurp(t_bunny_pixelarray *pix, int intensit, int diff)
{
  t_color	*pixels;
  t_color	*cpy;
  int		i;
  int		j;

  pixels = pix->pixels;
  cpy = bunny_malloc(HB * WB * sizeof(t_color));
  while (intensit--)
    {
      i = -1;
      while (++i < WB && (j = -1))
	while (++j < HB)
	  if (can_blurp(pix, i, j, diff))
	    cpy[getxy(i, j, pix)].full = RGBA_C(moy(pix, i, j, 0),
						moy(pix, i, j, 1),
						moy(pix, i, j, 2),
						moy(pix, i, j, 3));
	  else
	    cpy[getxy(i, j, pix)].full = pixels[getxy(i, j, pix)].full;
      i = -1;
      while (++i < WB * HB)
	pixels[i].full = cpy[i].full;
    }
  bunny_free(cpy);
}
