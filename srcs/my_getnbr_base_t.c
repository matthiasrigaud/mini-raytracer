/*
** my_getnbr_base_t.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:25:41 2016 Matthias RIGAUD
** Last update Mon May  9 15:25:42 2016 Matthias RIGAUD
*/

# include <raytracer.h>

int	error_t(char *str, char *base, int *str_s, int *minus)
{
  int	i;

  i = 0;
  while (str[i] != 0)
    {
      if (test(str[i], base) == 0 || str[0] == 0)
        {
          return (0);
        }
      if (str[i] == 45)
        {
          minus[1] = minus[1] + 1;
	  minus[0] = minus[0] + 1;
        }
      else if (str[i] == 43)
        {
	  minus[0] = minus[0] + 1;
        }
      i = i + 1;
    }
  *str_s = i;
  return (1);
}
