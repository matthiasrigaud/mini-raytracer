/*
** add_color.c for raytracer in /home/rigaud_b/rendu/raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed May 18 15:28:48 2016 Matthias RIGAUD
** Last update Sat May 21 15:35:33 2016 Matthias RIGAUD
*/

# include <raytracer.h>

uint		add_color(uint one, uint two)
{
  t_color	true_color_two;
  t_color	true_color_one;

  true_color_one.full = one;
  true_color_two.full = two;
  true_color_one.argb[0] = (unsigned)(true_color_one.argb[0])
    * (255 - (unsigned)(true_color_two.argb[3])) / 255;
  true_color_one.argb[0] += (unsigned)(true_color_two.argb[0])
    * (unsigned)(true_color_two.argb[3]) / 255;
  true_color_one.argb[1] = (unsigned)(true_color_one.argb[1])
    * (255 - (unsigned)(true_color_two.argb[3])) / 255;
  true_color_one.argb[1] += (unsigned)(true_color_two.argb[1])
    * (unsigned)(true_color_two.argb[3]) / 255;
  true_color_one.argb[2] = (unsigned)(true_color_one.argb[2])
    * (255 - (unsigned)(true_color_two.argb[3])) / 255;
  true_color_one.argb[2] += (unsigned)(true_color_two.argb[2])
    * (unsigned)(true_color_two.argb[3]) / 255;
  true_color_one.argb[3] = 255;
  return (true_color_one.full);
}

uint		fus_col(uint	color,
			int	reflect,
			uint	lighting)
{
  t_col		true_color;
  t_col		true_lighting;
  double	final;

  true_color.full = color;
  true_lighting.full = lighting;
  if (!(true_lighting.argb[0]) &&
      !(true_lighting.argb[1]) &&
      !(true_lighting.argb[2]))
    final = 0;
  else
    final = reflect;
  if (final > 100)
    final = 100;
  true_color.argb[3] *= (double)(final / 100.0);
  return (true_color.full);
}
