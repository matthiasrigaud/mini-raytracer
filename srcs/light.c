/*
** light.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:24:12 2016 Matthias RIGAUD
** Last update Sat May 21 20:00:43 2016 Matthias RIGAUD
*/

# include <raytracer.h>

static double	get_coef(t_lgt *light, t_pxl *pos, t_drt *drt, t_3dp *rfct)
{
  double	i[3];

  IA = light->ia * pos->material->ka;
  ID = light->id * pos->material->kd * (drt->vec.x * pos->nrm.x + drt->vec.y *
					pos->nrm.y + drt->vec.z * pos->nrm.z);
  IS = light->is * pos->material->ks * pow(rfct->x * pos->view.x + rfct->y *
					   pos->view.y + rfct->z * pos->view.z,
					   pos->material->alpha);
  drt->vec.x *= -1;
  drt->vec.y *= -1;
  drt->vec.z *= -1;
  return (IA + ID + IS);
}

unsigned int	get_light(t_color	*color,
			  t_pxl		*pos,
			  t_obj		*objs,
			  t_lgt		*light)
{
  t_drt		drt;
  t_3dp		rfct;
  t_3dp		tmp;
  double	i;
  double	norme;
  int		j;

  norme = light_ray(&drt, pos, &light->pos, &rfct);
  i = get_coef(light, pos, &drt, &rfct);
  j = -1;
  while (objs[++j].obj)
    if (objs[j].fonc(&drt, objs[j].obj, &light->pos, &tmp) && drt.vec.x *
  	(tmp.x - light->pos.x) >= 0 && drt.vec.y * (tmp.y - light->pos.y) >= 0
  	&& drt.vec.z * (tmp.z - light->pos.z) >= 0 &&
  	norme > sqrt(CAR(tmp.x - light->pos.x) + CAR(tmp.y - light->pos.y)
  		     + CAR(tmp.z - light->pos.z)) + 0.000000001)
      {
  	color->argb[0] /= 1 + 0.1 * (double)(objs[j].color.argb[3] / 255.0);
  	color->argb[1] /= 1 + 0.1 * (double)(objs[j].color.argb[3] / 255.0);
  	color->argb[2] /= 1 + 0.1 * (double)(objs[j].color.argb[3] / 255.0);
      }
  color->argb[0] = BORN(color->argb[0] * i, 255, 0);
  color->argb[1] = BORN(color->argb[1] * i, 255, 0);
  color->argb[2] = BORN(color->argb[2] * i, 255, 0);
  return (color->full);
}

double		light_ray(t_drt	*drt,
			  t_pxl	*pos,
			  t_3dp	*light,
			  t_3dp *reflect)
{
  double	norme;
  double	norme2;
  double	ang_cos;

  drt->pnt.x = pos->pos.x;
  drt->pnt.y = pos->pos.y;
  drt->pnt.z = pos->pos.z;
  drt->vec.x = -pos->pos.x + light->x;
  drt->vec.y = -pos->pos.y + light->y;
  drt->vec.z = -pos->pos.z + light->z;
  norme = sqrt(CAR(drt->vec.x) + CAR(drt->vec.y) + CAR(drt->vec.z));
  drt->vec.x /= norme;
  drt->vec.y /= norme;
  drt->vec.z /= norme;
  norme2 = sqrt(CAR(pos->view.x) + CAR(pos->view.y) + CAR(pos->view.z));
  pos->view.x /= norme2;
  pos->view.y /= norme2;
  pos->view.z /= norme2;
  ang_cos = (drt->vec.x * pos->nrm.x + drt->vec.y * pos->nrm.y
	     + drt->vec.z * pos->nrm.z);
  reflect->x = 2 * ang_cos * pos->nrm.x - drt->vec.x;
  reflect->y = 2 * ang_cos * pos->nrm.y - drt->vec.y;
  reflect->z = 2 * ang_cos * pos->nrm.z - drt->vec.z;
  return (norme);
}
