/*
** eye_scr.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Sun May 22 12:45:28 2016 Matthias RIGAUD
** Last update Sun May 22 22:54:12 2016 Matthias RIGAUD
*/

# include <raytracer.h>

void		rotate_x(t_3dp *vec, t_3dp *scr, t_3dp *eye)
{
  double	angle;
  t_3dp		new;

  angle = acos(CAR(scr->x - eye->x) / ((scr->x - eye->x)
				       * sqrt(CAR(scr->x - eye->x)
					      + CAR(scr->z - eye->z))));
  new.x = cos(angle) * vec->x + sin(angle) * vec->z;
  new.z = -sin(angle) * vec->x + cos(angle) * vec->z;
  vec->x = new.x;
  vec->z = new.z;
}

void		rotate_y(t_3dp *vec, t_3dp *scr, t_3dp *eye)
{
  double	angle;
  t_3dp		new;

  angle = acos(CAR(scr->x - eye->x) / ((scr->x - eye->x)
				       * sqrt(CAR(scr->x - eye->x)
					      + CAR(scr->y - eye->y))));
  new.x = cos(angle) * vec->x - sin(angle) * vec->y;
  new.y = sin(angle) * vec->x + cos(angle) * vec->y;
  vec->x = new.x;
  vec->y = new.y;
}

int	eye_scr_init(int fd, t_scr *screen, t_3dp *eye, int *is_init)
{
  if ((*is_init))
    return (puterror("Bad file format\nmultiple eye_scr\n"));
  *is_init = 1;
  if (get_point(fd, eye) ||
      get_point(fd, &screen->pos))
    return (puterror("Bad file format\neye_scr\n"));
  if (eye->x == screen->pos.x && eye->y == screen->pos.y &&
      eye->z == screen->pos.z)
    return (puterror("Bad file format\neye_scr\neye == scr\n"));
  screen->vcx.x = 0;
  screen->vcx.y = 0;
  screen->vcx.z = (VA(screen->pos.x - eye->x)) / 2000.0;
  screen->vcy.x = 0;
  screen->vcy.y = (VA(screen->pos.x - eye->x)) / 2000.0;
  screen->vcy.z = 0;
  rotate_x(&screen->vcx, &screen->pos, eye);
  rotate_y(&screen->vcy, &screen->pos, eye);
  return (0);
}
