/*
** my_char_isnum.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:24:47 2016 Matthias RIGAUD
** Last update Mon May  9 15:24:49 2016 Matthias RIGAUD
*/

int	my_char_isnum(char c)
{
  if (c >= 48 && c <= 57)
    return (1);
  return (0);
}
