/*
** reflect.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Fri May 20 18:12:32 2016 Matthias RIGAUD
** Last update Sat May 21 00:28:56 2016 Matthias RIGAUD
*/

# include <raytracer.h>

uint	get_reflect(t_lgt *light, t_pxl *pxl, t_obj *objs, t_pnt *fnl)
{
  t_col	white;
  uint	tmp_col;
  uint	cur_col;
  uint	next_col;
  uint	fnl_c;
  uint	lgtg;
  t_drt	reflect;

  fnl_c = BLACK;
  white.full = WHITE;
  light_ray(&reflect, pxl, &light->pos, &reflect.vec);
  cur_col = get_light(&fnl->color, pxl, objs, light);
  lgtg = get_light(&white, pxl, objs, light);
  next_col = fus_col(get_pixel(&reflect, &fnl->pnt.pnt, objs, light),
		     fnl->reflect, lgtg);
  tmp_col = add_color(cur_col, next_col);
  fnl_c = add_color(fnl_c, tmp_col);
  return (fnl_c);
}
