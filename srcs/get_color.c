/*
** get_color.c for raytracer in /home/rigaud_b/rendu/raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed May 18 14:54:46 2016 Matthias RIGAUD
** Last update Fri May 20 18:54:51 2016 Matthias RIGAUD
*/

# include <raytracer.h>

static void	copy_pnt(t_pnt	*dest,
			 t_pnt	*origin)
{
  dest->pnt.pnt.x = origin->pnt.pnt.x;
  dest->pnt.pnt.y = origin->pnt.pnt.y;
  dest->pnt.pnt.z = origin->pnt.pnt.z;
  dest->pnt.vec.x = origin->pnt.vec.x;
  dest->pnt.vec.y = origin->pnt.vec.y;
  dest->pnt.vec.z = origin->pnt.vec.z;
  dest->color.full = origin->color.full;
  dest->reflect = origin->reflect;
}

static void	swap_pnt(t_pnt *one, t_pnt *two)
{
  t_pnt		tmp;

  copy_pnt(&tmp, one);
  copy_pnt(one, two);
  copy_pnt(two, &tmp);
}

static void	sort_tab(t_pnt *tab, t_3dp *eye, int size)
{
  int		i;
  int		j;
  int		far;

  i = -1;
  while (++i < size)
    {
      j = i;
      far = j;
      while (++j < size)
	{
	  if (DIST(tab[j].pnt.pnt, *eye) < DIST(tab[far].pnt.pnt, *eye))
	    swap_pnt(&tab[j], &tab[far]);
	}
    }
}

static int	get_points(t_drt *drt, t_pnt *fnl, t_3dp *orig, t_obj *objs)
{
  int		i;
  int		j;
  t_pnt		tmp;

  i = -1;
  j = 0;
  while (objs[++i].obj)
    if (objs[i].fonc(drt, objs[i].obj, orig, &tmp.pnt.pnt))
      {
	objs[i].norm(&tmp.pnt, objs[i].obj);
	tmp.color.full = objs[i].color.full;
	tmp.reflect = objs[i].reflect;
	copy_pnt(&fnl[j], &tmp);
	fnl[j].material = &objs[i].material;
	++j;
      }
  sort_tab(fnl, orig, j);
  return (j);
}

uint		get_pixel(t_drt *drt, t_3dp *orig, t_obj *objs, t_lgt *light)
{
  static int	rec;
  t_pnt		fnl[NB_OBJ];
  uint		fnl_c;
  t_pxl		pxl;
  int		j;

  j = get_points(drt, fnl, orig, objs);
  fnl_c = BLACK;
  ++rec;
  pxl.view.x = -drt->vec.x;
  pxl.view.y = -drt->vec.y;
  pxl.view.z = -drt->vec.z;
  while (--j >= 0 && cp_3dp(&pxl.nrm, &fnl[j].pnt.vec) &&
	 cp_3dp(&pxl.pos, &fnl[j].pnt.pnt) && (pxl.material = fnl[j].material))
    if (!fnl[j].reflect || rec > 3)
      fnl_c = add_color(fnl_c, get_light(&fnl[j].color, &pxl, objs, light));
    else
      fnl_c = get_reflect(light, &pxl, objs, &fnl[j]);
  rec--;
  return (fnl_c);
}
