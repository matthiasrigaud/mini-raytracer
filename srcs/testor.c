/*
** testor.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:28:32 2016 Matthias RIGAUD
** Last update Sat May 21 20:06:51 2016 Matthias RIGAUD
*/

# include <raytracer.h>

int		who_is_better(t_3dp	*pos,
			      t_3dp	*pos2,
			      t_3dp	*distscr,
			      t_3dp	*eye)
{
  double	eyescr;
  double	eyepos;
  double	eyepos2;

  eyescr = sqrt(CAR(distscr->x) + CAR(distscr->y) + CAR(distscr->z));
  eyepos = sqrt(CAR(pos->x - eye->x)
		+ CAR(pos->y - eye->y)
		+ CAR(pos->z - eye->z));
  eyepos2 = sqrt(CAR(pos2->x - eye->x)
		 + CAR(pos2->y - eye->y)
		 + CAR(pos2->z - eye->z));
  if ((eyepos <= eyescr || distscr->x * (pos->x - eye->x) < 0) &&
      (eyepos2 <= eyescr || distscr->x * (pos2->x - eye->x) < 0))
    return (0);
  if (eyepos > eyescr && distscr->x * (pos->x - eye->x) > 0 &&
      (eyepos2 >= eyepos || eyepos2 <= eyescr
       || distscr->x * (pos2->x - eye->x) < 0))
    return (1);
  pos->x = pos2->x;
  pos->y = pos2->y;
  pos->z = pos2->z;
  return (1);
}

int		sphere_testor(t_drt *droite, void *obj, t_3dp *eye, t_3dp *pos)
{
  t_spr		*spr;
  double	a;
  double	b;
  double	c;
  double	delta;
  t_3dp		pos2;

  spr = obj;
  a = CAR(droite->vec.x) + CAR(droite->vec.y) + CAR(droite->vec.z);
  b = 2 * droite->vec.x * droite->pnt.x + 2 * droite->vec.y * droite->pnt.y
    + 2 * droite->vec.z * droite->pnt.z - 2 * droite->vec.x * spr->center.x
    - 2 * droite->vec.y * spr->center.y - 2 * droite->vec.z * spr->center.z;
  c = -2 * droite->pnt.x * spr->center.x - 2 * droite->pnt.y * spr->center.y
    - 2 * droite->pnt.z * spr->center.z + CAR(droite->pnt.x)
    + CAR(droite->pnt.y) + CAR(droite->pnt.z) + CAR(spr->center.x)
    + CAR(spr->center.y) + CAR(spr->center.z) - CAR(spr->radius);
  if ((delta = CAR(b) - 4 * a * c) < 0)
    return (0);
  pos->x = droite->vec.x * (c = ((-b - sqrt(delta)) / (2 * a))) + droite->pnt.x;
  pos->y = droite->vec.y * c + droite->pnt.y;
  pos->z = droite->vec.z * c + droite->pnt.z;
  pos2.x = droite->vec.x * (c = ((-b + sqrt(delta)) / (2 * a))) + droite->pnt.x;
  pos2.y = droite->vec.y * c + droite->pnt.y;
  pos2.z = droite->vec.z * c + droite->pnt.z;
  return (who_is_better(pos, &pos2, &droite->vec, eye));
}

int		plane_testor(t_drt *droite, void *obj, t_3dp *eye, t_3dp *pos)
{
  t_pln		*pln;
  t_3dp		n;
  double	d;
  double	t;

  pln = obj;
  n.x = (pln->fp.y - pln->sp.y) * (pln->fp.z - pln->tp.z)
    - (pln->fp.z - pln->sp.z) * (pln->fp.y - pln->tp.y);
  n.y = (pln->fp.z - pln->sp.z) * (pln->fp.x - pln->tp.x)
    - (pln->fp.x - pln->sp.x) * (pln->fp.z - pln->tp.z);
  n.z = (pln->fp.x - pln->sp.x) * (pln->fp.y - pln->tp.y)
    - (pln->fp.y - pln->sp.y) * (pln->fp.x - pln->tp.x);
  d = -(n.x * pln->fp.x + n.y * pln->fp.y + n.z * pln->fp.z);
  t = (n.x * droite->pnt.x + n.y * droite->pnt.y + n.z * droite->pnt.z + d)
    / -(n.x * droite->vec.x + n.y * droite->vec.y + n.z * droite->vec.z);
  pos->x = droite->vec.x * t  + droite->pnt.x;
  pos->y = droite->vec.y * t  + droite->pnt.y;
  pos->z = droite->vec.z * t  + droite->pnt.z;
  if (n.x * pos->x + n.y * pos->y + n.z * pos->z + d < 0.000001 &&
      n.x * pos->x + n.y * pos->y + n.z * pos->z + d > -0.000001)
    return (who_is_better(pos, pos, &droite->vec, eye));
  return (0);
}

int	tor_testor(t_drt *drt, void *obj, t_3dp *eye, t_3dp *pos)
{
  t_tor	*tor;
  t_e4d	eq;
  int	nb_pos;
  int	returner;
  t_3dp	p[4];

  tor = obj;
  tor_testor_calc(&eq, drt, tor);
  eq.sol = 0;
  eq.s1 = 0;
  eq.s2 = 0;
  eq.s3 = 0;
  eq.s4 = 0;
  tor_testor_get_solutions(&eq);
  nb_pos = tor_testor_get_pnt(p, drt, &eq);
  returner = 0;
  pos->x = eye->x;
  pos->y = eye->y;
  pos->z = eye->z;
  while (--nb_pos >= 0)
    returner = who_is_better(pos, &p[nb_pos], &drt->vec, eye);
  return (returner);
}

int	cylindre_testor(t_drt *drt, void *obj, t_3dp *eye, t_3dp *pos)
{
  t_cld		*cld;
  double	a;
  double	b;
  double	c;
  double	delta;
  t_3dp		pos2;

  cld = obj;
  drt->pnt.x -= cld->drt.pnt.x;
  drt->pnt.y -= cld->drt.pnt.y;
  drt->pnt.z -= cld->drt.pnt.z;
  c = cylindre_calc(&a, &b, cld, drt);
  drt->pnt.x += cld->drt.pnt.x;
  drt->pnt.y += cld->drt.pnt.y;
  drt->pnt.z += cld->drt.pnt.z;
  if ((delta = CAR(b) - 4 * a * c) < 0)
    return (0);
  pos->x = drt->vec.x * (c = ((-b - sqrt(delta)) / (2 * a))) + drt->pnt.x;
  pos->y = drt->vec.y * c + drt->pnt.y;
  pos->z = drt->vec.z * c + drt->pnt.z;
  pos2.x = drt->vec.x * (c = ((-b + sqrt(delta)) / (2 * a))) + drt->pnt.x;
  pos2.y = drt->vec.y * c + drt->pnt.y;
  pos2.z = drt->vec.z * c + drt->pnt.z;
  return (who_is_better(pos, &pos2, &drt->vec, eye));
}
