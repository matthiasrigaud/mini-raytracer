/*
** testor_ext.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Sat May 21 22:07:16 2016 Matthias RIGAUD
** Last update Sat May 21 23:14:28 2016 Matthias RIGAUD
*/

# include <raytracer.h>

double		calc_cone(double *a, double *b, t_drt *drt, t_cne *cne)
{
  double	c;
  double	angle;

    angle = CAR(tan(cne->angle));
  *a = CAR(X) * AX('x', cne->axe) + CAR(Y) * AX('y', cne->axe)
    + CAR(Z) * AX('z', cne->axe);
  *b = 2 * (X * PX * AX('x', cne->axe) + Y * PY * AX('y', cne->axe)
	    + Z * PZ * AX('z', cne->axe));
  c = CAR(PX) * AX('x', cne->axe) + CAR(PY) * AX('y', cne->axe)
    + CAR(PZ) * AX('z', cne->axe);
  return (c);
}

int	cone_testor(t_drt *drt, void *obj, t_3dp *eye, t_3dp *pos)
{
  t_cne		*cne;
  double	a;
  double	b;
  double	c;
  double	delta;
  t_3dp		pos2;

  cne = obj;
  drt->pnt.x -= cne->pnt.x;
  drt->pnt.y -= cne->pnt.y;
  drt->pnt.z -= cne->pnt.z;
  c = calc_cone(&a, &b, drt, cne);
  drt->pnt.x += cne->pnt.x;
  drt->pnt.y += cne->pnt.y;
  drt->pnt.z += cne->pnt.z;
  if ((delta = CAR(b) - 4 * a * c) < 0)
    return (0);
  pos->x = drt->vec.x * (c = ((-b - sqrt(delta)) / (2 * a))) + drt->pnt.x;
  pos->y = drt->vec.y * c + drt->pnt.y;
  pos->z = drt->vec.z * c + drt->pnt.z;
  pos2.x = drt->vec.x * (c = ((-b + sqrt(delta)) / (2 * a))) + drt->pnt.x;
  pos2.y = drt->vec.y * c + drt->pnt.y;
  pos2.z = drt->vec.z * c + drt->pnt.z;
  return (who_is_better(pos, &pos2, &drt->vec, eye));
}
