/*
** other.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:26:34 2016 Matthias RIGAUD
** Last update Mon May  9 15:41:48 2016 Matthias RIGAUD
*/

# include <raytracer.h>

int	stlen(char *str)
{
  return ((!(*str)) ? (0) : (1 + stlen(&str[1])));
}

int	puterror(char *msg)
{
  write(2, msg, stlen(msg));
  return (1);
}

int	my_strcmp(char *s1, char *s2, int nb)
{
  int	i;

  i = -1;
  if (!nb)
    while (s1[++i])
      if (s1[i] != s2[i])
	return (s1[i] - s2[i]);
  if (nb)
    while (s1[++i] || i < nb)
      if (s1[i] != s2[i])
	return (s1[i] - s2[i]);
  return (0);
}

int	free_comment(char *str)
{
  bunny_free(str);
  return (1);
}
