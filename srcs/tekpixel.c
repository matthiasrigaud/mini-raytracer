/*
** tekpixel.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:19:35 2016 Matthias RIGAUD
** Last update Fri May 20 16:45:41 2016 Matthias RIGAUD
*/

# include <raytracer.h>

void		tekpixel(t_pixar *pix, t_pos *pos, unsigned int color)
{
  t_color	*tab;
  t_color	true_color;
  int		i;

  tab = pix->pixels;
  true_color.full = color;
  i = pix->clipable.buffer.width * pos->y + pos->x;
  if (pos->x >= 0 && pos->y >= 0 &&
      pos->y < pix->clipable.buffer.height &&
      pos->x < pix->clipable.buffer.width)
    {
      tab[i].argb[0] = (unsigned)(tab[i].argb[0])
	* (255 - (unsigned)(true_color.argb[3])) / 255;
      tab[i].argb[0] += (unsigned)(true_color.argb[0])
	* (unsigned)(true_color.argb[3]) / 255;
      tab[i].argb[1] = (unsigned)(tab[i].argb[1])
	* (255 - (unsigned)(true_color.argb[3])) / 255;
      tab[i].argb[1] += (unsigned)(true_color.argb[1])
	* (unsigned)(true_color.argb[3]) / 255;
      tab[i].argb[2] = (unsigned)(tab[i].argb[2])
	* (255 - (unsigned)(true_color.argb[3])) / 255;
      tab[i].argb[2] += (unsigned)(true_color.argb[2])
	* (unsigned)(true_color.argb[3]) / 255;
      tab[i].argb[3] = 255;
    }
}
