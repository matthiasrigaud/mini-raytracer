/*
** normale.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Thu May 12 15:50:26 2016 Matthias RIGAUD
** Last update Sat May 21 22:59:57 2016 Matthias RIGAUD
*/

# include <raytracer.h>

void		plane_normale(t_drt *point, void *obj)
{
  t_pln		*pln;
  double	dist;

  pln = obj;
  point->vec.x = (pln->fp.y - pln->sp.y) * (pln->fp.z - pln->tp.z)
    - (pln->fp.z - pln->sp.z) * (pln->fp.y - pln->tp.y);
  point->vec.y = (pln->fp.z - pln->sp.z) * (pln->fp.x - pln->tp.x)
    - (pln->fp.x - pln->sp.x) * (pln->fp.z - pln->tp.z);
  point->vec.z = (pln->fp.x - pln->sp.x) * (pln->fp.y - pln->tp.y)
    - (pln->fp.y - pln->sp.y) * (pln->fp.x - pln->tp.x);
  dist = sqrt(CAR(point->vec.x) + CAR(point->vec.y) + CAR(point->vec.z));
  point->vec.x /= dist;
  point->vec.y /= dist;
  point->vec.z /= dist;
}

void		sphere_normale(t_drt *point, void *obj)
{
  t_spr		*spr;
  double	dist;

  spr = obj;
  point->vec.x = point->pnt.x - spr->center.x;
  point->vec.y = point->pnt.y - spr->center.y;
  point->vec.z = point->pnt.z - spr->center.z;
  dist = sqrt(CAR(point->vec.x) + CAR(point->vec.y) + CAR(point->vec.z));
  point->vec.x /= dist;
  point->vec.y /= dist;
  point->vec.z /= dist;
}

void		tor_normale(t_drt *point, void *obj)
{
  t_tor		*tor;
  double	dist;

  tor = obj;
  point->vec.x = 4 * point->pnt.x * (CAR(point->pnt.x) + CAR(point->pnt.y) +
				     CAR(point->pnt.z) + CAR(R) - CAR(PR)) -
    8 * CAR(R) * point->pnt.x;
  point->vec.y = 4 * point->pnt.y * (CAR(point->pnt.x) + CAR(point->pnt.y) +
				     CAR(point->pnt.z) + CAR(R) - CAR(PR));
  point->vec.z = 4 * point->pnt.z * (CAR(point->pnt.x) + CAR(point->pnt.y) +
				     CAR(point->pnt.z) + CAR(R) - CAR(PR)) -
    8 * CAR(R) * point->pnt.z;
  dist = sqrt(CAR(point->vec.x) + CAR(point->vec.y) + CAR(point->vec.z));
  point->vec.x /= dist;
  point->vec.y /= dist;
  point->vec.z /= dist;
}

void	cylindre_normale(t_drt *drt, void *obj)
{
  t_cld		*cld;
  double	dist;
  double	a;
  double	b;

  cld = obj;
  a = CAR(CX) + CAR(CY) + CAR (CZ);
  b = 2 * (CPX * CX + CPY * CY + CPZ * CZ - PX * CX - PY * CY - PZ * CZ);
  drt->vec.x = (drt->pnt.x - (CX * (-b / (2 * a)) + CPX));
  drt->vec.y = (drt->pnt.y - (CY * (-b / (2 * a)) + CPY));
  drt->vec.z = (drt->pnt.z - (CZ * (-b / (2 * a)) + CPZ));
  dist = sqrt(CAR(drt->vec.x) + CAR(drt->vec.y) + CAR(drt->vec.z));
  drt->vec.x /= dist;
  drt->vec.y /= dist;
  drt->vec.z /= dist;
}

void	cone_normale(t_drt *drt, void *obj)
{
  t_cne		*cne;
  t_drt		c_drt;
  double	dist;
  double	a;
  double	b;

  cne = obj;
  c_drt.pnt.x = cne->pnt.x;
  c_drt.pnt.y = cne->pnt.y;
  c_drt.pnt.z = cne->pnt.z;
  c_drt.vec.x = drt->pnt.x - c_drt.pnt.x;
  c_drt.vec.y = drt->pnt.y - c_drt.pnt.y;
  c_drt.vec.z = drt->pnt.z - c_drt.pnt.z;
  a = CAR(COX) + CAR(COY) + CAR (COZ);
  b = 2 * (COPX * COX + COPY * COY + COPZ * COZ - PX * COX - PY * COY
	   - PZ * COZ);
  drt->vec.x = (COX * (-b / (2 * a)) + COPX) + GETAX('x', cne->axe) - cne->pnt.x;
  drt->vec.y = (COY * (-b / (2 * a)) + COPY) + GETAX('y', cne->axe) - cne->pnt.y;
  drt->vec.z = (COZ * (-b / (2 * a)) + COPZ) + GETAX('z', cne->axe) - cne->pnt.z;
  dist = sqrt(CAR(drt->vec.x) + CAR(drt->vec.y) + CAR(drt->vec.z));
  drt->vec.x /= dist;
  drt->vec.y /= dist;
  drt->vec.z /= dist;
}
