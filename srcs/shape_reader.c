/*
** shape_reader.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Thu May 12 14:47:57 2016 Matthias RIGAUD
** Last update Sun May 22 14:35:11 2016 Matthias RIGAUD
*/

# include <raytracer.h>

int	get_next_param(int fd, char *param, int *error)
{
  char	*s;
  int	e;
  int	value;

  e = 0;
  value = 0;
  *error = 0;
  if (!(s = get_next_line(fd, 0, &e)) && e)
    return (-(*error = puterror("Error read")));
  if (my_strcmp(param, s, stlen(param)))
    *error = puterror("Bad File Format\n");
  if (!(value = my_getnbr(&s[stlen(param)], &e)) && e)
    *error = puterror("Bad File Format\n");
  bunny_free(s);
  if (*error)
    {
      puterror(param);
      puterror("\n");
      return (-1);
    }
  return (value);
}

int	read_torus(int fd, t_obj *objs)
{
  int	e;
  t_tor	*tor;

  if (!(tor = bunny_malloc(sizeof(t_tor))))
    return (puterror("Error Malloc"));
  if (get_point(fd, &tor->center) ||
      ((tor->r = get_next_param(fd, "r=", &e)) == -1 && e) ||
      ((tor->maj_r = get_next_param(fd, "R=", &e)) == - 1 && e) ||
      get_color(fd, &objs->color) ||
      ((objs->reflect = get_next_param(fd, "reflect=", &e)) == -1 && e) ||
      material_reader(fd, &objs->material))
    return (puterror("torus\n"));
  objs->reflect = BR(objs->reflect, 100, 0);
  objs->fonc = &tor_testor;
  objs->norm = &tor_normale;
  objs->obj = tor;
  objs->reflect = 0;
  return (0);
}

int	read_cylindre(int fd, t_obj *objs)
{
  int	e;
  t_cld	*cld;
  t_3dp	pnt;

  if (!(cld = bunny_malloc(sizeof(t_cld))))
    return (puterror("Error Malloc"));
  if (get_point(fd, &cld->drt.pnt) ||
      get_point(fd, &pnt) ||
      ((cld->radius = get_next_param(fd, "radius=", &e)) == -1 && e) ||
      get_color(fd, &objs->color) ||
      ((objs->reflect = get_next_param(fd, "reflect=", &e)) == -1 && e) ||
      material_reader(fd, &objs->material))
    return (puterror("cylindre\n"));
  cld->drt.vec.x = cld->drt.pnt.x - pnt.x;
  cld->drt.vec.y = cld->drt.pnt.y - pnt.y;
  cld->drt.vec.z = cld->drt.pnt.z - pnt.z;
  objs->reflect = BR(objs->reflect, 100, 0);
  objs->fonc = &cylindre_testor;
  objs->norm = &cylindre_normale;
  objs->obj = cld;
  return (0);
}

int	read_cone(int fd, t_obj *objs)
{
  int	e;
  t_cne	*cne;

  if (!(cne = bunny_malloc(sizeof(t_cne))))
    return (puterror("Error Malloc"));
  if (get_point(fd, &cne->pnt) ||
      get_axe(fd, &cne->axe) ||
      ((cne->angle = get_next_param(fd, "angle=", &e)) == -1 && e) ||
      get_color(fd, &objs->color) ||
      ((objs->reflect = get_next_param(fd, "reflect=", &e)) == -1 && e) ||
      material_reader(fd, &objs->material))
    return (puterror("cone\n"));
  cne->angle = ((BR(cne->angle, 360, 0)) * M_PI * 2) / 360.0;
  objs->reflect = BR(objs->reflect, 100, 0);
  objs->fonc = &cone_testor;
  objs->norm = &cone_normale;
  objs->obj = cne;
  return (0);
}

int	read_others_shapes(int fd, t_obj *objs, char *field, int *error)
{
  if (!my_strcmp(field, "[TORUS]", 0))
    *error = read_torus(fd, objs);
  else if (!my_strcmp(field, "[CYLINDRE]", 0))
    *error = read_cylindre(fd, objs);
  else if (!my_strcmp(field, "[CONE]", 0))
    *error = read_cone(fd, objs);
  else if (!my_strcmp(field, "[PLANE]", 0))
    *error = read_plane(fd, objs);
  else
    return (0);
  return (1);
}
