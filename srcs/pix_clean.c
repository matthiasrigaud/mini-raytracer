/*
** pix_clean.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:28:16 2016 Matthias RIGAUD
** Last update Mon May  9 15:29:27 2016 Matthias RIGAUD
*/

# include <raytracer.h>

void            pix_clean(t_pixar *pix, unsigned int color)
{
  t_pos		pos;

  pos.y = -1;
  while (++pos.y < pix->clipable.buffer.height)
    {
      pos.x = -1;
      while (++pos.x < pix->clipable.buffer.width)
	tekpixel(pix, &pos, color);
    }
}
