/*
** drawer.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:22:51 2016 Matthias RIGAUD
** Last update Wed May 18 17:34:37 2016 Matthias RIGAUD
*/

# include <raytracer.h>

t_drt	*droite_param(t_drt	*drt,
		      t_3dp	*eye,
		      t_scr	*screen,
		      t_pos	*pos)
{
  drt->pnt.x = screen->pos.x + screen->vcx.x * (pos->x - screen->size.x / 2)
    +  screen->vcy.x * (pos->y - screen->size.y / 2);
  drt->pnt.y = screen->pos.y + screen->vcx.y * (pos->x - screen->size.x / 2)
    +  screen->vcy.y * (pos->y - screen->size.y / 2);
  drt->pnt.z = screen->pos.z + screen->vcx.z * (pos->x - screen->size.x / 2)
    +  screen->vcy.z * (pos->y - screen->size.y / 2);
  drt->vec.x = drt->pnt.x - eye->x;
  drt->vec.y = drt->pnt.y - eye->y;
  drt->vec.z = drt->pnt.z - eye->z;
  return (drt);
}

void		drawer(t_box *b, t_3dp *eye, t_scr *screen, t_obj *objs)
{
  t_pos		p;
  t_drt		drt;

  p.y = -1;
  while (++p.y < b->pix->clipable.buffer.height && (p.x = -1))
    while (++p.x < b->pix->clipable.buffer.width &&
	   (droite_param(&drt, eye, screen, &p)))
      tekpixel(b->pix, &p, get_pixel(&drt, eye, objs, &b->light));
}
