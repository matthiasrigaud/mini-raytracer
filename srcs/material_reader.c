/*
** material_reader.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Fri May 20 19:11:39 2016 Matthias RIGAUD
** Last update Fri May 20 19:38:12 2016 Matthias RIGAUD
*/

# include <raytracer.h>

int	material_reader(int fd, t_mtr *material)
{
  int	e;

  e = 0;
  if (((material->ka = get_next_param(fd, "ka=", &e)) == - 1 && e) ||
      ((material->kd = get_next_param(fd, "kd=", &e)) == - 1 && e) ||
      ((material->ks = get_next_param(fd, "ks=", &e)) == - 1 && e) ||
      ((material->alpha = get_next_param(fd, "alpha=", &e)) == - 1 && e))
    return (puterror("material\n"));
  material->ka = BR(material->ka, 100, 0) / 100;
  material->kd = BR(material->kd, 100, 0) / 100;
  material->ks = BR(material->ks, 100, 0) / 100;
  material->alpha = MIN(material->alpha, 0);
  return (0);
}
