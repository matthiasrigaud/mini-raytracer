/*
** main.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:24:30 2016 Matthias RIGAUD
** Last update Sun May 22 12:51:13 2016 Matthias RIGAUD
*/

#include <raytracer.h>

void	mover(t_box *b)
{
  b->eye.x += b->zoom * b->zomer.x;
  b->eye.y += b->zoom * b->zomer.y;
  b->eye.z += b->zoom * b->zomer.z;
  b->screen.pos.x += b->zoom * b->zomer.x;
  b->screen.pos.y += b->zoom * b->zomer.y;
  b->screen.pos.z += b->zoom * b->zomer.z;
  b->eye.x += b->decal * b->decaler.x;
  b->eye.y += b->decal * b->decaler.y;
  b->eye.z += b->decal * b->decaler.z;
  b->screen.pos.x += b->decal * b->decaler.x;
  b->screen.pos.y += b->decal * b->decaler.y;
  b->screen.pos.z += b->decal * b->decaler.z;
  b->eye.x += b->mont * b->monter.x;
  b->eye.y += b->mont * b->monter.y;
  b->eye.z += b->mont * b->monter.z;
  b->screen.pos.x += b->mont * b->monter.x;
  b->screen.pos.y += b->mont * b->monter.y;
  b->screen.pos.z += b->mont * b->monter.z;
}

t_res	mainloop(void *data)
{
  t_box	*b;

  b = data;
  if (b->zoom || b->decal || b->mont)
    b->is_draw = 0;
  if (!b->is_draw)
    {
      mover(b);
      pix_clean(b->pix, BLACK);
      drawer(b, &b->eye, &b->screen, b->objs);
      blurp(b->pix, 2, 2);
      b->is_draw = 1;
    }
  bunny_blit(&b->win->buffer, &b->pix->clipable, NULL);
  bunny_display(b->win);
  return (GO_ON);
}

void	screen_init(t_box *b)
{
  if (!b->is_screen_init)
    {
      b->eye.x = 0;
      b->eye.y = 0;
      b->eye.z = 0;
      b->screen.pos.x = 1000;
      b->screen.pos.y = 0;
      b->screen.pos.z = 0;
      b->screen.vcx.x = 0;
      b->screen.vcx.y = 0;
      b->screen.vcx.z = (VA(b->screen.pos.x - b->eye.x)) / 2000.0;
      b->screen.vcy.x = 0;
      b->screen.vcy.y = (VA(b->screen.pos.x - b->eye.x)) / 2000.0;
      b->screen.vcy.z = 0;
    }
  b->screen.size.x = b->pix->clipable.buffer.width;
  b->screen.size.y = b->pix->clipable.buffer.height;
}

void		init_one(t_box *b)
{
  double	norme;

  b->win = bunny_start_style(WIDTH, HEIGHT, OPTN, TITLE);
  b->pix = bunny_new_pixelarray(WIDTH, HEIGHT);
  b->zoom = 0;
  b->decal = 0;
  b->mont = 0;
  screen_init(b);
  norme = sqrt(CAR(b->screen.pos.x - b->eye.x) + CAR(b->screen.pos.y - b->eye.y)
	       + CAR(b->screen.pos.z - b->eye.z));
  b->zomer.x = ((b->screen.pos.x - b->eye.x) / norme) * 200;
  b->zomer.y = ((b->screen.pos.y - b->eye.y) / norme) * 200;
  b->zomer.z = ((b->screen.pos.z - b->eye.z) / norme) * 200;
  b->decaler.x = 200 * b->screen.vcx.x;
  b->decaler.y = 200 * b->screen.vcx.y;
  b->decaler.z = 200 * b->screen.vcx.z;
  b->monter.x = 200 * b->screen.vcy.x;
  b->monter.y = 200 * b->screen.vcy.y;
  b->monter.z = 200 * b->screen.vcy.z;
  b->is_draw = 0;
  bunny_set_loop_main_function(mainloop);
  bunny_set_key_response(getkey);
}

int	main(int ac, char **av)
{
  t_box	b;
  int	i;

  bunny_set_maximum_ram(MAX_RAM);
  if (ac != 2)
    return (puterror("Please enter one and only one .scn\n"));
  b.is_screen_init = 0;
  if (file_reader(av[1], b.objs, &b))
    return (1);
  init_one(&b);
  bunny_loop(b.win, 200, &b);
  bunny_stop(b.win);
  bunny_delete_clipable(&b.pix->clipable);
  i = -1;
  while (b.objs[++i].obj)
    bunny_free(b.objs[i].obj);
  bunny_set_memory_check(true);
  return (0);
}
