/*
** other_calc.c for raytracer in /home/rigaud_b/rendu/raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Fri May 13 10:51:43 2016 Matthias RIGAUD
** Last update Fri May 13 15:06:16 2016 Matthias RIGAUD
*/

# include <raytracer.h>

double		trd_d_eq(double a, double b, double c, double d)
{
  double	p;
  double	q;
  double	delta;

  p = -CAR(b) / (3 * CAR(a)) + c / a;
  q = (b / (27 * a)) * ((2 * CAR(b)) / CAR(a) - (9 * c) / a) + d / a;
  delta = CAR(q) + (4 * P3(p)) / 27;
  if (delta > 0)
    return (CBRT((-q + sqrt(delta)) / 2) + CBRT((-q - sqrt(delta)) / 2)
	    - b / (3 * a));
  else if (IS_NULL(delta) && IS_NULL(p))
    return (CBRT(-q) - b / (3 * a));
  else if (IS_NULL(delta) && !IS_NULL(p))
    return ((3 * q) / p - b / (3 * a));
  else
    return (2 * (CBRT(-q / 2) * cos(sqrt(-delta) / 6)) - b / (3 * a));
  return (0);
}

t_sol		*snd_d_eq(double a, double b, double c,  t_sol *sol)
{
  double	delta;

  delta = CAR(b) - 4 * a * c;
  if (delta < 0)
    return (NULL);
  sol->s1 = (-b - sqrt(delta)) / (2 * a);
  sol->s2 = (-b + sqrt(delta)) / (2 * a);
  return (sol);
}
