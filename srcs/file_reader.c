/*
** file_reader.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:23:16 2016 Matthias RIGAUD
** Last update Sun May 22 14:36:48 2016 Matthias RIGAUD
*/

# include <raytracer.h>

int	get_point(int fd, t_3dp *pnt)
{
  char	*str;
  int	error;

  if (!(str = get_next_line(fd, 0, &error)) && error)
    return (puterror("Error read"));
  if (my_strcmp("x=", str, 2))
    return (puterror("Bad File Format\npoint\n"));
  if (!(pnt->x = my_getnbr(&str[2], &error)) && error)
    return (puterror("Bad File Format\npoint\n"));
  bunny_free(str);
  if (!(str = get_next_line(fd, 0, &error)) && error)
    return (puterror("Error read"));
  if (my_strcmp("y=", str, 2))
    return (puterror("Bad File Format\npoint\n"));
  if (!(pnt->y = my_getnbr(&str[2], &error)) && error)
    return (puterror("Bad File Format\npoint\n"));
  bunny_free(str);
  if (!(str = get_next_line(fd, 0, &error)) && error)
    return (puterror("Error read"));
  if (my_strcmp("z=", str, 2))
    return (puterror("Bad File Format\npoint\n"));
  if (!(pnt->z = my_getnbr(&str[2], &error)) && error)
    return (puterror("Bad File Format\npoint\n"));
  bunny_free(str);
  return (0);
}

int	get_color(int fd, t_color *color)
{
  char	*s;
  int	e;

  if (!(s = get_next_line(fd, 0, &e)) && e)
    return (puterror("Error read"));
  if (my_strcmp("color=0x", s, 8))
    return (puterror("Bad File Format\ncolor\n"));
  if (!(color->full = my_getnbr_base(&s[8], "0123456789ABCDEF", &e)) && e)
    return (puterror("Bad File Format\ncolor\n"));
  bunny_free(s);
  return (0);
}

int	read_sphere(int fd, t_obj *objs)
{
  int	e;
  t_spr	*spr;

  if (!(spr = bunny_malloc(sizeof(t_spr))))
    return (puterror("Error Malloc"));
  if (get_point(fd, &spr->center) ||
      ((spr->radius = get_next_param(fd, "radius=", &e)) == -1 && e) ||
      get_color(fd, &objs->color) ||
      ((objs->reflect = get_next_param(fd, "reflect=", &e)) == -1 && e) ||
      material_reader(fd, &objs->material))
    return (puterror("sphere\n"));
  objs->reflect = BR(objs->reflect, 100, 0);
  objs->fonc = &sphere_testor;
  objs->norm = &sphere_normale;
  objs->obj = spr;
  return (0);
}

int	read_plane(int fd, t_obj *objs)
{
  t_pln	*pln;
  int	e;

  e = 0;
  if (!(pln = bunny_malloc(sizeof(t_pln))))
    return (puterror("Error Malloc"));
  if (get_point(fd, &pln->fp) ||
      get_point(fd, &pln->sp) ||
      get_point(fd, &pln->tp) ||
      get_color(fd, &objs->color) ||
      ((objs->reflect = get_next_param(fd, "reflect=", &e)) == -1 && e) ||
      material_reader(fd, &objs->material))
    return (puterror("plane\n"));
  objs->reflect = BR(objs->reflect, 100, 0);
  objs->fonc = &plane_testor;
  objs->norm = &plane_normale;
  objs->obj = pln;
  return (0);
}

int	file_reader(char *path, t_obj *objs, t_box *b)
{
  int	fd;
  int	error;
  int	nb_objs;
  char	*field;

  if ((fd = open(path, O_RDONLY)) == 1)
    return (puterror("Error Open\n"));
  error = 0;
  nb_objs = 0;
  while (!error && (field = get_next_line(fd, 0, &error))
	 && free_comment(field) && (field = get_next_line(fd, 0, &error))
	 && nb_objs < NB_OBJ)
    {
      if (!my_strcmp(field, "[SPHERE]", 0))
	error = read_sphere(fd, &objs[nb_objs++]);
      else if (!my_strcmp(field, "[EYE-SCR]", 0))
	error = eye_scr_init(fd, &b->screen, &b->eye, &b->is_screen_init);
      else if (!my_strcmp(field, "[LIGHT]", 0))
	error = read_light(fd, &b->light);
      else if (!read_others_shapes(fd, &objs[nb_objs++], field, &error))
	error = puterror("Bad File Format\nfield\n");
      bunny_free(field);
    }
  objs[nb_objs].obj = NULL;
  return (error);
}
