/*
** cylindre_calc.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Sat May 21 20:04:02 2016 Matthias RIGAUD
** Last update Sat May 21 20:05:31 2016 Matthias RIGAUD
*/

# include <raytracer.h>

double		cylindre_calc(double *a, double *b, t_cld *cld, t_drt *drt)
{
  double	c;

  *a = CAR(X) + CAR(Y) + CAR(Z) - ((CAR(X * CX + Y * CY + Z * CZ)) /
				   (CAR(CX) + CAR(CY) + CAR(CZ)));
  *b = 2 * (X * PX + Y * PY + Z * PZ - (((CX * PX + CY * PY + CZ * PZ) *
					 (CX * X + CY * Y + CZ * Z)) /
					(CAR(CX) + CAR(CY) + CAR(CZ))));
  c = CAR(PX) + CAR(PY) + CAR(PZ) - CAR(cld->radius) -
    (2 * (CX * CY * PX * PY + CX * CZ * PX * PZ + CZ * CY * PZ * PY) +
     CAR(CX) * CAR(PX) + CAR(CY) * CAR(PY) + CAR(CZ) * CAR(PZ)) /
    (CAR(CX) + CAR(CY) + CAR(CZ));
  return (c);
}
