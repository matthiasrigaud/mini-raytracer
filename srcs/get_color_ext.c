/*
** get_color_ext.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Thu May 19 19:09:50 2016 Matthias RIGAUD
** Last update Thu May 19 19:11:30 2016 Matthias RIGAUD
*/

# include <raytracer.h>

int	cp_3dp(t_3dp *dest, t_3dp *src)
{
  dest->x = src->x;
  dest->y = src->y;
  dest->z = src->z;
  return (1);
}
