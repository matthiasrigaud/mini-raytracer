/*
** my_getnbr.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:25:58 2016 Matthias RIGAUD
** Last update Mon May  9 15:26:02 2016 Matthias RIGAUD
*/

# include <raytracer.h>

int	my_getnbr(char *str, int *error)
{
  int	i;

  *error = 0;
  i = -1;
  while (str[++i] == 43 || str[i] == 45);
  if (i > 1)
    {
      *error = 1;
      return (0);
    }
  while (my_char_isnum(str[i]) == 1)
    {
      i = i + 1;
    }
  if (i < stlen(str))
    {
      *error = 1;
      return (0);
    }
  return ((int)(my_getnbr_base(str, "0123456789", error)));
}
