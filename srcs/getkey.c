/*
** getkey.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:23:50 2016 Matthias RIGAUD
** Last update Mon May  9 15:23:51 2016 Matthias RIGAUD
*/

# include <raytracer.h>

t_res	getkey(t_state state, t_key key, void *data)
{
  t_box	*b;

  b = data;
  if (state == GO_DOWN && key == BKS_ESCAPE)
    return (EXIT_ON_SUCCESS);
  if (state == GO_DOWN && key == BKS_UP)
    b->zoom = 1;
  else if (state == GO_DOWN && key == BKS_DOWN)
    b->zoom = -1;
  else if (state == GO_UP && (key == BKS_UP || key == BKS_DOWN))
    b->zoom = 0;
  else if (state == GO_DOWN && key == BKS_RIGHT)
    b->decal = 1;
  else if (state == GO_DOWN && key == BKS_LEFT)
    b->decal = -1;
  else if (state == GO_UP && (key == BKS_RIGHT || key == BKS_LEFT))
    b->decal = 0;
  else if (state == GO_DOWN && key == BKS_PAGEUP)
    b->mont = -1;
  else if (state == GO_DOWN && key == BKS_PAGEDOWN)
    b->mont = 1;
  else if (state == GO_UP && (key == BKS_PAGEUP || key == BKS_PAGEDOWN))
    b->mont = 0;
  return (GO_ON);
}
