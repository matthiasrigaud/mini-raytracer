/*
** get_axe.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Sat May 21 21:57:38 2016 Matthias RIGAUD
** Last update Sat May 21 22:01:13 2016 Matthias RIGAUD
*/

# include <raytracer.h>

int	get_axe(int fd, char *axe)
{
  char	*str;
  int	error;

  if (!(str = get_next_line(fd, 0, &error)) && error)
    return (puterror("Error read"));
  if (my_strcmp("axe=", str, 4) ||
      stlen(str) != 5 ||
      (str[4] != 'x' && str[4] != 'y' && str[4] != 'z'))
    return (puterror("Bad File Format\naxe\n"));
  *axe = str[4];
  return (0);
}
