/*
** get_next_line.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:21:28 2016 Matthias RIGAUD
** Last update Mon May  9 15:43:30 2016 Matthias RIGAUD
*/

# include <raytracer.h>

static int	is_10_in(t_buf *buf)
{
  int		i;

  i = -1;
  while (++i < buf->size)
    {
      if (buf->str[i] == 10)
	return (i);
    }
  return (-1);
}

static int	purge_buffer(t_buf *buf)
{
  char		*new_buf;
  int		i;
  int		j;

  if (!(new_buf = bunny_malloc(READ_SIZE + 1)))
    return (1);
  i = is_10_in(buf);
  j = i + 1;
  while (++i < buf->size)
    new_buf[i - j] = buf->str[i];
  buf->size = i - j;
  bunny_free(buf->str);
  buf->str = new_buf;
  return (0);
}

static int	add_n_car(t_buf *dest, t_buf *src, int n)
{
  char		*ndest;
  int		i;

  if (n < 0)
    n = src->size;
  if (!(ndest = bunny_malloc(dest->size + n + 1)))
    return (1);
  i = -1;
  while (++i < dest->size)
    ndest[i] = dest->str[i];
  bunny_free(dest->str);
  dest->str = ndest;
  i = -1;
  while (++i < n)
    {
      if (i >= src->size)
	{
	  dest->size += src->size;
	  return (0);
	}
      dest->str[dest->size + i] = src->str[i];
    }
  dest->size += n;
  return (0);
}

static int	reader(t_buf *buffer, t_buf *line, const int fd, int *error)
{
  if (add_n_car(line, buffer, buffer->size) && (*error = 1))
    return (1);
  if ((buffer->size = read(fd, buffer->str, READ_SIZE)) < 0 && (*error = 1))
    return (1);
  if (!buffer->size && !line->size)
    return (1);
  if (!buffer->size)
    return (2);
  return (0);
}

char		*get_next_line(const int fd, int erase, int *error)
{
  static t_buf	buffer;
  t_buf		line;
  int		ret;

  *error = 0;
  if (!(line.str = bunny_malloc(1)) && (*error = 1))
    return (NULL);
  line.size = 0;
  if (!buffer.str || erase)
    {
      if (!(buffer.str = bunny_malloc(READ_SIZE + 1)) && (*error = 1))
	return (NULL);
      buffer.size = 0;
    }
  while (is_10_in(&buffer) < 0)
    if ((ret = reader(&buffer, &line, fd, error)) == 1)
      return (NULL);
    else if (ret == 2)
      break;
  if (add_n_car(&line, &buffer, is_10_in(&buffer)) && (*error = 1))
    return (NULL);
  if (purge_buffer(&buffer) && (*error = 1))
    return (NULL);
  line.str[line.size] = 0;
  return (line.str);
}
