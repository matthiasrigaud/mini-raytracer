/*
** tor_calc.c for raytracer in /home/rigaud_b/rendu/raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Fri May 13 14:19:05 2016 Matthias RIGAUD
** Last update Fri May 13 17:20:04 2016 Matthias RIGAUD
*/

# include <raytracer.h>

/* void	tor_testor_calc(t_e4d *eq, t_drt *drt, t_tor *tor) */
/* { */
/*   eq->a = P4(X) + P4(Y) + P4(Z) + 2 * CAR(X) * CAR(Y) + 2 * CAR(X) * CAR(Z) */
/*     + 2 * CAR(Z) * CAR(Y); */
/*   eq->b = 4 * P3(X) * PX + 4 * P3(Y) * PY + 4 * P3(Z) * PZ + 4 * CAR(X) * Y */
/*     * PY + 4 * CAR(Y) * X * PX + 4 * CAR(X) * Z * PZ + 4 * CAR(Z) * X * PX */
/*     + 4 * CAR(Y) * Z * PZ + 4 * CAR(Z) * Y * PY; */
/*   eq->c = 6 * CAR(X) * CAR(PX) + 6 * CAR(Y) * CAR(PY) + 6 * CAR(Z) * CAR(PZ) */
/*     - 2 * CAR(R) * CAR(X) - 2 * CAR(PR) * CAR(X) - 2 * CAR(R) * CAR(Y) - 2 */
/*     * CAR(PR) * CAR(Y) + 2 * CAR(R) * CAR(Z) - 2 * CAR(PR) * CAR(Z) */
/*     + 2 * CAR(X) * CAR(PY) + 8 * X * Y * PX * PY + 2 * CAR(Y) * CAR(PX) */
/*     + 2 * CAR(X) * CAR(PZ) + 8 * X * Z * PX * PZ + 2 * CAR(Z) * CAR(PX) */
/*     + 2 * CAR(Z) * CAR(PY) + 8 * Z * Y * PZ * PY + 2 * CAR(Y) * CAR(PZ); */
/*   eq->d = 4 * X * P3(PX) + 4 * Y * P3(PY) + 4 * Z * P3(PZ) - 4 * CAR(R) * X */
/*     * PX - 4 * CAR(PR) * X * PX - 4 * CAR(R) * Y * PY - 4 * CAR(PR) * Y * PY */
/*     + 4 * CAR(R) * Z * PZ - 4 * CAR(PR) * Z * PZ + 4 * X * PX * CAR(PY) */
/*     + 4 * Y * PY * CAR(PX) + 4 * X * PX * CAR(PZ) + 4 * Z * PZ * CAR(PX) */
/*     + 4 * Y * PY * CAR(PZ) + 4 * Z * PZ * CAR(PY); */
/*   eq->e = P4(PX) + P4(PY) + P4(PZ) + P4(R) + P4(PR) - 2 * CAR(R) * CAR(PX) - 2 */
/*     * CAR(PR) * CAR(PX) - 2 * CAR(R) * CAR(PY) - 2 * CAR(PR) * CAR(PY) + 2 * */
/*     CAR(R) * CAR(PZ) - 2 * CAR(PR) * CAR(PZ) - 2 * CAR(PR) * CAR(R) */
/*     + 2 * CAR(PX) * CAR(PY) + 2 * CAR(PX) * CAR(PZ) + 2 * CAR(PZ) * CAR(PY); */
/*   eq->p = (-3 * CAR(eq->b)) / (8 * CAR(eq->a)) + eq->c / eq->a; */
/*   eq->q = P3(eq->b) / (8 * P3(eq->a)) - (eq->b * eq->c) / (2 * CAR(eq->a)) */
/*     + eq->d / eq->a; */
/*   eq->r = -3 * P4(eq->b / (4 * eq->a)) + (CAR(eq->b) * eq->c) / */
/*     (16 * P3(eq->a)) + (eq->b * eq->d) / (4 * CAR(eq->a)) + eq->e / eq->a; */
/* } */

void	tor_testor_calc(t_e4d *eq, t_drt *drt, t_tor *tor)
{
  eq->a = CAR(CAR(X) + CAR(Y) + CAR(Z));
  eq->b = 4 * (CAR(X) + CAR(Y) + CAR(Z)) * (X * PX + Y * PY + Z * PZ);
  eq->c = 2 * (CAR(X) + CAR(Y) + CAR(Z)) *
    (CAR(PX) + CAR(PY) + CAR(PZ) + CAR(R) - CAR(PR)) +
    CAR(X * PX + Y * PY + Z * PZ)
    - 4 * CAR(R) * (CAR(X) + CAR(Z));
  eq->d = 4 * (X * PX + Y * PY + Z * PZ) *
    (CAR(PX) + CAR(PY) + CAR(PZ) + CAR(R) - CAR(PR))
    - 8 * CAR(R) * (X * PX + Z * PZ);
  eq->e = (CAR(PX) + CAR(PY) + CAR(PZ) + CAR(R) - CAR(PR)) - 4 * CAR(R)
    * (CAR(PX) + CAR (PZ));
  eq->p = (-3 * CAR(eq->b)) / (8 * CAR(eq->a)) + eq->c / eq->a;
  eq->q = P3(eq->b) / (8 * P3(eq->a)) - (eq->b * eq->c) / (2 * CAR(eq->a))
    + eq->d / eq->a;
  eq->r = -3 * P4(eq->b / (4 * eq->a)) + (CAR(eq->b) * eq->c) /
    (16 * P3(eq->a)) + (eq->b * eq->d) / (4 * CAR(eq->a)) + eq->e / eq->a;
}

void	tor_testor_get_solutions(t_e4d *eq)
{
  t_sol	sol;

  if (IS_NULL(eq->q) && P2(eq->p) - 4 * eq->r >= 0 && (eq->sol = 3))
    {
      eq->s1 = sqrt((-eq->p - sqrt(P2(eq->p) - 4 * eq->r)) / 2) - B / (4 * A);
      eq->s2 = -sqrt((-eq->p - sqrt(P2(eq->p) - 4 * eq->r)) / 2) - B / (4 * A);
      eq->s3 = sqrt((-eq->p + sqrt(P2(eq->p) - 4 * eq->r)) / 2) - B / (4 * A);
      eq->s4 = -sqrt((-eq->p + sqrt(P2(eq->p) - 4 * eq->r)) / 2) - B / (4 * A);
    }
  else if (!IS_NULL(eq->q))
    {
      Y0 = trd_d_eq(8, -4 * eq->p, -8 * eq->r, 4 * eq->p * eq->r - P2(eq->q));
      eq->a0 = sqrt(-eq->p + 2 * eq->y0);
      eq->b0 = -eq->p / (2 * eq->a0);
      if (snd_d_eq(1, -eq->a0, eq->y0 - eq->b0, &sol) && (eq->sol += 1))
	{
	  eq->s1 = sol.s1 - B / (4 * A);
	  eq->s2 = sol.s2 - B / (4 * A);
	}
      if (snd_d_eq(1, eq->a0, eq->y0 + eq->b0, &sol) && (eq->sol += 2))
	{
	  eq->s3 = sol.s1 - B / (4 * A);
	  eq->s4 = sol.s2 - B / (4 * A);
	}
    }
}

int	tor_testor_get_pnt(t_3dp *p, t_drt *drt, t_e4d *eq)
{
  int	nb_pos;

    nb_pos = 0;
  if ((eq->sol & 1))
    {
      p[nb_pos].x = X * eq->s1 + PX;
      p[nb_pos].y = Y * eq->s1 + PY;
      p[nb_pos++].z = Z * eq->s1 + PZ;
      p[nb_pos].x = X * eq->s2 + PX;
      p[nb_pos].y = Y * eq->s2 + PY;
      p[nb_pos++].z = Z * eq->s2 + PZ;
    }
  if ((eq->sol & 2))
    {
      p[nb_pos].x = X * eq->s3 + PX;
      p[nb_pos].y = Y * eq->s3 + PY;
      p[nb_pos++].z = Z * eq->s3 + PZ;
      p[nb_pos].x = X * eq->s4 + PX;
      p[nb_pos].y = Y * eq->s4 + PY;
      p[nb_pos++].z = Z * eq->s4 + PZ;
    }
  return (nb_pos);
}
