/*
** light_reader.c for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Thu May 19 19:16:49 2016 Matthias RIGAUD
** Last update Thu May 19 19:27:20 2016 Matthias RIGAUD
*/

# include <raytracer.h>

int	read_light(int fd, t_lgt *light)
{
  int	e;

  if (get_point(fd, &light->pos) ||
      ((light->ia = get_next_param(fd, "ia=", &e)) == -1 && e) ||
      ((light->id = get_next_param(fd, "id=", &e)) == -1 && e) ||
      ((light->is = get_next_param(fd, "is=", &e)) == -1 && e))
    return (puterror("light\n"));
  light->ia = BORN(light->ia, 100, 0) / 100.0;
  light->id = BORN(light->id, 100, 0) / 100.0;
  light->is = BORN(light->is, 100, 0) / 100.0;
  return (0);
}
