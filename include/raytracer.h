/*
** raytracer.h for raytracer in /home/rigaud_b/rendu/gfx_raytracer2/include
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Mon May  9 15:18:44 2016 Matthias RIGAUD
** Last update Sun May 22 14:34:43 2016 Matthias RIGAUD
*/

#ifndef RAYTRACER_H_
# define RAYTRACER_H_

/*
** ## Includes ##
*/

# include <lapin.h>
# include <math.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include <get_next_line.h>

# include <stdio.h>

/*
** ## Define ##
*/

/*
** max_ram
*/

# define MAX_RAM		2000000042

/*
** window
*/

# define HEIGHT			768
# define WIDTH			1024
# define TITLE			"Raytracer2"
# define OPTN			TITLEBAR | CLOSE_BUTTON

/*
** others
*/

# define CAR(x)			((x) * (x))
# define P2(x)			(CAR(x))
# define P3(x)			(CAR(x) * (x))
# define P4(x)			((CAR(x)) * (CAR(x)))
# define CBRT(x)		(-exp(log(-(x)) / 3))
# define IS_NULL(x)		((x > -0.0000001 && x < 0.0000001) ? (1) : (0))
# define VA(x)			((x < 0) ? (-(x)) : (x))
# define WB			pix->clipable.buffer.width
# define HB			pix->clipable.buffer.height
# define RGBA_C(r, g, b, a)	((a << 24) | (b << 16) | (g << 8) | r)

# define X			drt->vec.x
# define Y			drt->vec.y
# define Z			drt->vec.z
# define PX			drt->pnt.x
# define PY			drt->pnt.y
# define PZ			drt->pnt.z
# define R			tor->maj_r
# define PR			tor->r
# define Y0			eq->y0
# define A			eq->a
# define B			eq->b
# define CPX			cld->drt.pnt.x
# define CPY			cld->drt.pnt.y
# define CPZ			cld->drt.pnt.z
# define CX			cld->drt.vec.x
# define CY			cld->drt.vec.y
# define CZ			cld->drt.vec.z
# define COPX			c_drt.pnt.x
# define COPY			c_drt.pnt.y
# define COPZ			c_drt.pnt.z
# define COX			c_drt.vec.x
# define COY			c_drt.vec.y
# define COZ			c_drt.vec.z

# define IA			i[0]
# define ID			i[1]
# define IS			i[2]

# define DIST2(a, b)		(CAR((a).y - (b).y) + CAR((a).z - (b).z))
# define DIST(a, b)		(sqrt(CAR((a).x - (b).x) + DIST2(a, b)))

# define NB_OBJ			1000

# define MAX_DIST		5000.0

# define MAX(x, y)		((x > y) ? (y) : (x))
# define MIN(x, y)		((x < y) ? (y) : (x))
# define BORN(x, y, z)		(MAX(MIN(x, z), y))
# define BR(x, y, z)		BORN(x, y, z)

# define GETAX(x, y)		((x == y) ? (1) : (0))

# define AX(x, y)		((x == y) ? (-angle) : (1))

/*
** ## Typedef && struct ##
*/

typedef t_bunny_position        t_pos;
typedef t_bunny_pixelarray      t_pixar;
typedef t_bunny_window          t_win;
typedef t_bunny_response        t_res;
typedef t_bunny_event_state     t_state;
typedef t_bunny_keysym          t_key;
typedef t_bunny_ini             t_ini;
typedef t_bunny_ini_scope       t_scp;
typedef t_bunny_sound           t_snd;
typedef t_bunny_music           t_msc;
typedef t_bunny_effect          t_fct;
typedef unsigned int		uint;
typedef t_color			t_col;

typedef struct	s_3dp
{
  double	x;
  double	y;
  double	z;
}		t_3dp;

typedef struct	s_e4d
{
  double	a;
  double	b;
  double	c;
  double	d;
  double	e;
  double	p;
  double	q;
  double	r;
  double	a0;
  double	b0;
  double	y0;
  int		sol;
  double	s1;
  double	s2;
  double	s3;
  double	s4;
}		t_e4d;

typedef struct	s_sol
{
  double	s1;
  double	s2;
}		t_sol;

typedef struct	s_lgt
{
  t_3dp		pos;
  double	id;
  double	ia;
  double	is;
}		t_lgt;

typedef struct	s_mtr
{
  double	ka;
  double	kd;
  double	ks;
  int		alpha;
}		t_mtr;

/*
** shapes
*/

typedef struct	s_cne
{
  t_3dp		pnt;
  double	angle;
  char		axe;
}		t_cne;

typedef struct	s_pxl
{
  t_3dp		nrm;
  t_3dp		pos;
  t_3dp		view;
  t_mtr		*material;
}		t_pxl;

typedef struct	s_drt
{
  t_3dp		pnt;
  t_3dp		vec;
}		t_drt;

typedef struct	s_pnt
{
  t_drt		pnt;
  t_col		color;
  int		reflect;
  t_mtr		*material;
}		t_pnt;

typedef struct	s_cld
{
  t_drt		drt;
  double	radius;
}		t_cld;

typedef struct	s_spr
{
  double	radius;
  t_3dp		center;
}		t_spr;

typedef struct	s_tor
{
  t_3dp		center;
  double	r;
  double	maj_r;
}		t_tor;

typedef struct	s_pln
{
  t_3dp		fp;
  t_3dp		sp;
  t_3dp		tp;
}		t_pln;

/*
** screen
*/

typedef struct	s_scr
{
  t_3dp		pos;
  t_3dp		vcx;
  t_3dp		vcy;
  t_pos		size;
}		t_scr;

/*
** shape list
*/

typedef struct	s_obj
{
  int		(*fonc)(t_drt *droite, void *obj, t_3dp *eye, t_3dp *pos);
  void		(*norm)(t_drt *point, void *obj);
  t_color	color;
  int		reflect;
  t_mtr		material;
  void		*obj;
}		t_obj;

/*
** main struct
*/

typedef struct	s_box
{
  t_win		*win;
  t_pixar	*pix;
  t_3dp		eye;
  t_lgt		light;
  t_scr		screen;
  int		is_draw;
  t_3dp		zomer;
  t_3dp		decaler;
  t_3dp		monter;
  int		zoom;
  int		decal;
  int		mont;
  t_obj		objs[NB_OBJ];
  int		is_screen_init;
}		t_box;

/*
** ## Functions ##
*/

t_res	getkey(t_state state, t_key key, void *data);

int	sphere_testor(t_drt *droite, void *obj, t_3dp *eye, t_3dp *pos);
int	plane_testor(t_drt *droite, void *obj, t_3dp *eye, t_3dp *pos);
int	tor_testor(t_drt *droite, void *obj, t_3dp *eye, t_3dp *pos);
int	cylindre_testor(t_drt *droite, void *obj, t_3dp *eye, t_3dp *pos);
int	cone_testor(t_drt *droite, void *obj, t_3dp *eye, t_3dp *pos);
int	who_is_better(t_3dp *pos, t_3dp *pos2, t_3dp *distscr, t_3dp *eye);

double	cylindre_calc(double *a, double *b, t_cld *clf, t_drt *drt);
void	tor_testor_calc(t_e4d *eq, t_drt *drt, t_tor *tor);
void	tor_testor_get_solutions(t_e4d *eq);
int	tor_testor_get_pnt(t_3dp *p, t_drt *drt, t_e4d *eq);

double	trd_d_eq(double a, double b, double c, double d);
t_sol	*snd_d_eq(double a, double b, double c, t_sol *sol);

void	plane_normale(t_drt *point, void *obj);
void	sphere_normale(t_drt *point, void *obj);
void	tor_normale(t_drt *point, void *obj);
void	cylindre_normale(t_drt *point, void *obj);
void	cone_normale(t_drt *point, void *obj);

void	tekpixel(t_pixar *pix, t_pos *pos, unsigned int color);
void	pix_clean(t_pixar *pix, unsigned int color);
void	drawer(t_box *b, t_3dp *eye, t_scr *screen, t_obj *objs);
void	blurp(t_pixar *pix, int intensit, int diff);

int	puterror(char *msg);
int	my_strcmp(char *s1, char *s2, int nb);
int     my_char_isnum(char c);
uint	my_getnbr_base(char *str, char*base, int *error);
int     error_t(char *str, char *base, int *str_s, int *minus);
int	stlen(char *str);
int	my_getnbr(char *str, int *error);
int	eye_scr_init(int fd, t_scr *screen, t_3dp *eye, int *is_init);
int	read_plane(int fd, t_obj *objs);

int	file_reader(char *path, t_obj *objs, t_box *b);
int	free_comment(char *str);
int	read_others_shapes(int fd, t_obj *objs, char *field, int *error);
int	get_next_param(int fd, char *param, int *error);
int	get_point(int fd, t_3dp *pnt);
int	get_color(int fd, t_color *color);
int	read_light(int fd, t_lgt *light);
int	material_reader(int fd, t_mtr *material);
int	get_axe(int fd, char *axe);

double	light_ray(t_drt *droite, t_pxl *pos, t_3dp *light, t_3dp *reflect);
void	tekray(const t_pos *screen_info, int *x, int *y, int *z);
uint	get_light(t_color *color, t_pxl *pos, t_obj *objs, t_lgt *light);
int     test(char str, char *base);

uint	get_pixel(t_drt *drt, t_3dp *origin, t_obj *objs, t_lgt *light);
uint	add_color(uint one, uint two);
uint	fus_col(uint color, int reflect, uint lighting);
uint	get_reflect(t_lgt *light, t_pxl *pxl, t_obj *objs, t_pnt *fnl);

int	cp_3dp(t_3dp *dest, t_3dp *src);

/*
** ## End ##
*/

#endif /* RAYTRACER_H_ */
