##
## Makefile for raytracer2 in /home/rigaud_b/rendu/gfx_raytracer2
## 
## Made by Matthias RIGAUD
## Login   <rigaud_b@epitech.eu>
## 
## Started on  Mon May  9 15:15:40 2016 Matthias RIGAUD
## Last update Sun May 22 14:38:12 2016 Matthias RIGAUD
##

RM		= \rm -f

CC		= gcc -g

NAME		= raytracer2

SRCS		= srcs/main.c \
		srcs/getkey.c \
		srcs/tekpixel.c \
		srcs/pix_clean.c \
		srcs/drawer.c \
		srcs/testor.c \
		srcs/blurp.c \
		srcs/file_reader.c \
		srcs/get_next_line.c \
		srcs/my_char_isnum.c \
		srcs/my_getnbr.c \
		srcs/my_getnbr_base.c \
		srcs/my_getnbr_base_t.c \
		srcs/other.c \
		srcs/light.c \
		srcs/shape_reader.c \
		srcs/normale.c \
		srcs/tor_calc.c \
		srcs/other_calc.c \
		srcs/add_color.c \
		srcs/get_color.c \
		srcs/get_color_ext.c \
		srcs/light_reader.c \
		srcs/reflect.c \
		srcs/material_reader.c \
		srcs/cylindre_calc.c \
		srcs/testor_ext.c \
		srcs/get_axe.c \
		srcs/eye_scr.c

OBJS		= $(SRCS:.c=.o)

SHEETIES	= $(OBJS) \
		$(SRCS:.c=.c~) \
		Makefile~

CFLAGS		= -W -Wall -Werror -Wextra -ansi -pedantic \
		-Iinclude \
		-I/home/${USER}/.froot/include

LIB		= -L/home/${USER}/.froot/lib \
		-llapin \
		-lsfml-audio \
		-lsfml-graphics \
		-lsfml-window \
		-lsfml-system \
		-lstdc++ \
		-ldl \
		-lm

all:		$(NAME)

$(NAME):	$(OBJS)
		@echo -e "\e[0m"
		@$(CC) $(OBJS) $(LIB) -o $(NAME)
		@echo -e "\e[32mAll done ! ==>\e[33m" $(NAME) "\e[32mcreated !\e[0m"

clean:
		@echo -en "\e[0mCleaning .o && .c~ files..."
		@$(RM) $(SHEETIES)
		@echo -e "	 [\e[32mOk !\e[0m]"

fclean:		clean
		@echo -en "\e[39mCleaning executable..."
		@$(RM) $(NAME)
		@echo -e "		 [\e[32mOk !\e[0m]"

re:		fclean all

comp:		re
		@echo -en "\e[0mCleaning .o && .c~ files..."
		@$(RM) $(SHEETIES)
		@echo -e "	 [\e[32mOk !\e[0m]"

.c.o:		%.c
		@$(CC) -c $< -o $@ $(CFLAGS) && \
		echo -e "\e[32m[OK]" $< "\e[93m"|| \
		echo -e "\e[91;5m[ERR]\e[25m" $< "\e[93m"

.PHONY:		all clean fclean re comp
